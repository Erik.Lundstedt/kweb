// include qt stuff
#include <QApplication>
#include <QMainWindow>
#include <QObject>
#include <QShortcut>
#include <QWebEngineView>
// include the lua library

// standard include
#include <iostream>
// end includes

struct config {
  std::string homeUrl = "https://erik.lundstedt.it/pages/newtab.html";
  // QString homeUrl="https://erik.lundstedt.it/pages/newtab.html";
  int winWidth = 700;
  int winHeight = 700;
  float opacity = 0.8;
  bool noModExit = false;
} cfg;




int main(int argc, char **argv) {
  QApplication app(argc, argv);
  QMainWindow window;
  QWebEngineView *view = new QWebEngineView();
  config conf = config{};

  window.setCentralWidget(view);
  window.resize(conf.winWidth, conf.winHeight);
  window.setWindowOpacity(conf.opacity);
  window.setWindowTitle(argv[0]);

  window.show();
  QUrl myUrl(argv[1]);
  if (!myUrl.isValid()) {
	myUrl.setUrl(QString::fromStdString(conf.homeUrl));
	if (!myUrl.isValid()) {
	  myUrl.setUrl("https://erik.lundstedt.it/pages/newtab.html");
	}
  }

  QShortcut shortcut(QKeySequence("Ctrl+Q"), &window);
  //	QShortcut shortcut1(QKeySequence("q"), &window);
  QObject::connect(&shortcut, SIGNAL(activated()), &window, SLOT(close()));
  // QObject::connect(&shortcut1, SIGNAL(activated()), &window, SLOT(close()));








  view->load(myUrl);

  return app.exec();
}
